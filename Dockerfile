# Use the Python runtime image provided by AWS
FROM public.ecr.aws/lambda/python:3.8

# Install updates and dependencies
RUN yum update -y && yum install -y openssl-devel

# Set the working directory in the container
WORKDIR /app

# Copy the Python script and requirements file into the container
COPY script.py requirements.txt /app/

# Install dependencies from the requirements file
RUN pip install --no-cache-dir -r requirements.txt
# Download the AWS Lambda RIE
ADD https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie /usr/local/bin/aws-lambda-rie

# Provide execute permissions for the AWS Lambda RIE
RUN chmod +x /usr/local/bin/aws-lambda-rie

# Specify the entrypoint as the AWS Lambda RIE
ENTRYPOINT [ "/usr/local/bin/aws-lambda-rie" ]

# Set the command to invoke the handler using the AWS Lambda RIC
CMD [ "python", "-m", "awslambdaric", "script.lambda_handler" ]
