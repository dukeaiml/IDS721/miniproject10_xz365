# Mini-Project10: AWS Lambda Deployment with Dockerized Hugging Face Rust Transformer
> Xingyu, Zhang (NetID: xz365)

This repository demonstrates how to deploy a Hugging Face Rust transformer model as a serverless application using AWS Lambda and Docker.

## Dockerizing Hugging Face Rust Transformer

### The Lambda Function ([script.py](./script.py))
The function is written in Python using the transformers library to perform sentiment analysis using the [cardiffnlp/twitter-roberta-base-sentiment-latest](https://huggingface.co/cardiffnlp/twitter-roberta-base-sentiment-latest) model. This function takes input text, analyzes its sentiment, and returns the result as a JSON response.

### Dockerize The Function
1. Prepare the [requirements.txt](requirements.txt) which includes all python packages needed for the function: `transformers`, `awslambdaric`, `urllib3==1.26.7` and `tensorflow`.
2. After creating the [requirements.txt](requirements.txt) file, we can use it to install the dependencies in our Docker container which allow our function to run sucessfully in the [Dockerfile](./Dockerfile). To be more specific:
    ```dockerfile
    # Use the Python runtime image provided by AWS
    FROM public.ecr.aws/lambda/python:3.8

    # Install updates and dependencies
    RUN yum update -y && yum install -y openssl-devel

    # Set the working directory in the container
    WORKDIR /app

    # Copy the Python script and requirements file into the container
    COPY script.py requirements.txt /app/

    # Install dependencies from the requirements file
    RUN pip install --no-cache-dir -r requirements.txt
    # Download the AWS Lambda RIE
    ADD https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie /usr/local/bin/aws-lambda-rie

    # Provide execute permissions for the AWS Lambda RIE
    RUN chmod +x /usr/local/bin/aws-lambda-rie

    # Specify the entrypoint as the AWS Lambda RIE
    ENTRYPOINT [ "/usr/local/bin/aws-lambda-rie" ]

    # Set the command to invoke the handler using the AWS Lambda RIC
    CMD [ "python", "-m", "awslambdaric", "script.lambda_handler" ]
    ```
3. Finally set up the environment, build and run the Docker container.
   ```bash
   # Build the Docker container
    docker build -t llm-test .

    # Run the Docker container
    docker run -p 9000:8080 llm-test
    ```
    The result should be like:
    ![build](./assets/local_host.png)

4. To test locally (In Windows Command Prompt, add `\` to escape double quotes within the JSON payload.):
   ```bash
   curl -XPOST "http://localhost:9000/2015-03-31/functions/function/invocations" -d '{"input_text": "Your input text here"}'
   ```
   An example of the test result is
   ![local test](./assets/local_test_result.png)


## Deploying Container to AWS Lambda
### Pushing A Docker Image To Amazon ECR
1. Firstly, create a repository in Amazon ECR named `sentimental-mosdel` in AWS Management Console, to be more specific, in the section called `Amazon ECR service`.
   
2. Tag the image with the command
    ```bash
    docker tag llm-test:tag 211125444211.dkr.ecr.us-east-1.amazonaws.com/sentimental-model:tag
    ```

3. Then push the Docker image to the container registry using the docker push command:
   ```bash
    docker push 211125444211.dkr.ecr.us-east-1.amazonaws.com/sentimental-model:tag
   ```

After all done, we can find the image in the `Amazon ECR service` section:
![ECR](./assets/ECR.png)

### Create And Deploy An AWS Lambda Function
Create the function in the section of `Lambda` in AWS Management Console and choose the "Container image" option as the runtime. Also specify the Docker image URI of your container image in the container registry. Then depoly it and you will get its endpoint:
![api](./assets/api.png)
