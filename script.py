import json
import tempfile
import os
from transformers import pipeline

# Load the sentiment analysis model
model = pipeline("sentiment-analysis", model="cardiffnlp/twitter-roberta-base-sentiment-latest")

# Set the temporary directory for transformers cache
tempfile.tempdir = "/tmp"
os.environ["TRANSFORMERS_CACHE"] = "/tmp"

def lambda_handler(event, context):
    """
    Lambda function handler for sentiment analysis.

    Parameters:
    event (dict): Input event containing text to analyze.
    context (object): Lambda execution context.

    Returns:
    dict: Response containing sentiment analysis result.
    """
    # Get input text from the event
    input_text = event.get("input_text", "")

    # Perform sentiment analysis
    sentiment_result = model(input_text)

    # Return the result
    return {
        "statusCode": 200,
        "body": json.dumps({"sentiment_result": sentiment_result}),
    }

if __name__ == "__main__":
    import sys

    # Check if any command line argument is provided
    if len(sys.argv) > 1:
        input_text = sys.argv[1]
    else:
        input_text = "Input the text"

    # Setting a dummy context
    context = {}

    # Create an event dictionary
    event = {"input_text": input_text}

    # Call the lambda handler
    result = lambda_handler(event, context)

    # Print the result
    print(result)
